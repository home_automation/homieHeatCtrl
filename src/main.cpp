#include <Homie.h>

#define firmwareVersion "0.0.2"
#define firmwareName "heatCtrl"

#define OFF 1
#define ON  0

#define LIVING_ROOM_GPIO    D3
#define BATH_ROOM_GPIO      D5
#define CHILDREN_ROOM_GPIO  D6
#define SLEEPING_ROOM_GPIO  D7

HomieNode nightMode("nightMode", "Night mode", "switch");

bool setGPIO(const String& value, uint8_t gpio) {
  if (value != "true" && value != "false") return false;

  bool on = (value == "true");
  digitalWrite(gpio, on ? ON : OFF);
  nightMode.setProperty("on").send(value);
  Homie.getLogger() << "DO is " << (on ? "on" : "off") << endl;

  return true;
}

bool lightOnHandler(const HomieRange& range, const String& value) {
  return setGPIO(value, LED_BUILTIN);
}

bool nightModeHandler_lr(const HomieRange& range, const String& value) {
  return setGPIO(value, LIVING_ROOM_GPIO);
}

void setupGPIO() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LIVING_ROOM_GPIO, OUTPUT);
  digitalWrite(LED_BUILTIN, OFF);
  digitalWrite(LIVING_ROOM_GPIO, OFF);

}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  setupGPIO();

  Homie_setFirmware(firmwareName, firmwareVersion);

  nightMode
    .advertise("ledon")
    .setName("LED ON")
    .setDatatype("boolean")
    .settable(lightOnHandler);

  nightMode
    .advertise("lron")
    .setName("Living room ON")
    .setDatatype("boolean")
    .settable(nightModeHandler_lr);

  Homie.setup();
}

void loop() {
  Homie.loop();
}
